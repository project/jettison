<?php
/**
 * API information for the Jettison module.
 *
 * Before using these hooks, you may also want to look at <code>jettison_js()</code>
 * and <code>jettison_css()</code>.
 * @file
 */

/**
 * Alter the list of JS files before they are removed from a page.
 *
 * Generally, JS items should be added with <code>jettison_js()</code>. Altering is 
 * really intended for last minute changes that cannot be done that way.
 *
 * @param &$list
 *  The list of JS items that are slated to be jettisoned.
 */
function hook_jettison_js_list_alter(&$list) {
  // Remove an item from the jettison list:
  unset($list['foo.js']);
}

/**
 * Alter the list of CSS files before they are removed from a page.
 *
 * This should be used to perform last-minute alterations of the list of CSS
 * files to jettison. It is not intended as a general-purpose way of adding
 * CSS files to be jettisoned. That should be done with <code>jettison_css()</code>.
 *
 * @param &$list
 *  The list of CSS items that are slated to be jettisoned.
 */
function hook_jettison_css_list_alter(&$list) {
  // Remove an item from the jettison list:
  unset($list['foo.css']);
}

/**
 * Your last chance to alter CSS before the template.
 *
 * Alter the list of CSS after it undesirables have been jettisoned, but 
 * before they are passed into the template.
 *
 * @param &$list
 *  The associative array of CSS items as they will be passed into the template system.
 */
function hook_jettison_css_postprocess_list_alter(&$list) {
  
}

/**
 * Your last chance to alter JS before it goes into the page template.
 *
 * This is called after items have been removed from the JS list, but before the clean list
 * has been passed into the template layer.
 *
 * @param &$list
 *  The associative array of all JavaScript, in all its nested glory.
 */
function hook_jettison_js_postprocess_list_alter(&$list) {
  
}
