<?php

/**
 * jettison.admin
 * 
 * Administration features for Jettison.
 *
 * This provides very high level generic support for jettison. Using the API gives one more 
 * control over exactly what is jettisoned.
 * @file
 */

/**
 * Admin settings form.
 */
function jettison_admin_form() {
  global $theme;
  
  $theme_objects = list_themes();
  $themes = array();
  foreach ($theme_objects as $theme_data) {
    $themes[$theme_data->name] = $theme_data->name;
  }
  
  $form['basic_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Basic Settings'),
    '#description' => t('Jettison unwanted CSS and JS. For more information, see !help.', array('!help' => l('the help page', 'admin/help/jettison'))),
  );
  
  
  $form['basic_settings']['jettison_active_themes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Themes to scan'),
    '#description' => t('Only the selected themes will be scanned for files to remove.'),
    '#options' => $themes,
    '#default_value' => variable_get('jettison_active_themes', array()),
    //'#required' => FALSE,
  );
  
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Remove Files'),
    '#description' => t('List the files you want removed. These will be removed from all of the themes checked below. Do not include a leading slash in paths. Example: !example', array('!example' => '<code>modules/user/user.css</code>')),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['filters']['jettison_js'] = array(
    '#type' => 'textarea',
    '#title' => t('JavaScript files'),
    '#description' => t('A list of JS files, with the full path, that should be removed. One per line.'),
    '#default_value' => variable_get('jettison_js', ''),
    '#rows' => 5,
    '#cols' => 60,
    '#required' => FALSE,
  );
  
  $form['filters']['jettison_css'] = array(
    '#type' => 'textarea',
    '#title' => t('CSS files'),
    '#description' => t('A list of CSS files, with full path, that should be removed. One per line.'),
    '#default_value' => variable_get('jettison_css', ''),
    '#rows' => 5,
    '#cols' => 60,
    '#required' => FALSE,
  );
  

  
  return system_settings_form($form);
}
