Jettison Unwanted CSS/JS

This module removes unwanted JS and CSS that is inserted by modules.

Written By

Brian Tully (btully)
Matt Butcher (mbutcher)

Sponsored by ConsumerSearch.com, a New York Times company.